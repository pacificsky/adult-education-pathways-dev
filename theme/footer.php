<?php
/**
 * Footer file common to all
 * templates
 *
 */
?>


<footer class="site__footer">
  <div class="wrapper">
    <?php dynamic_sidebar('footer-widgets'); ?>
  </div>
</footer>

<footer id="colophon">
  <div class="wrapper">
    <span class="copyright">&copy; All Rights Reserved Adult Education Pathways <?php echo date('Y'); ?></span>
    <span class="credit"><span>Design &amp; Development by</span><img src="https://pacificsky.co/logo/?variant=gray"></span>
  </div>
</footer>

<?php wp_footer(); ?>

<?php // </body> opens in header.php ?>
</body>
</html>
