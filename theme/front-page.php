<?php get_header(); ?>

	<section id="splash">
		<div class="wrapper" id="closer">
			<h2 class="splash__tagline">
				<?php MOZ_SVG::SVG('closer'); ?>
				<!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/closer.svg" alt="" /> -->
			</h2>
			<!-- <h2 class="splash__tagline"><span>Your Career is</span> <strong>Closer</strong> <span>than you think!</span></h2> -->
			<a href="#" class="button">Sign up now!</a>
		</div>
		<div class="wrapper" id="splash__nav">
			<h2>See how we Can Help</h2>
			<a href="#education">
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/education.jpg" alt="">
					<figcaption>Education</figcaption>
				</figure>
			</a>
			<a href="#support_services">
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/support_services.jpg" alt="">
					<figcaption>Support Services</figcaption>
				</figure>
			</a>
			<a href="#career">
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/career.jpg" alt="">
					<figcaption>Career</figcaption>
				</figure>
			</a>
		</div>
	</section>

	<section id="education">
		<div class="wrapper">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/education.jpg" alt=""></figure>
			<div class="text">
				<h2>It's Never Too Late for Education</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem similique cupiditate modi ad totam iste aliquid amet consequuntur velit nihil tenetur expedita ratione possimus deserunt, ducimus animi atque laboriosam, quia!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et similique, velit itaque ea quisquam repellat, beatae temporibus harum ducimus impedit excepturi aperiam dignissimos eaque error molestiae fugit. Sapiente ex, vitae?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque illo, tempora tenetur numquam rem, esse corrupti doloribus dolor sequi quis ipsum repudiandae. Itaque pariatur modi voluptatum corrupti dolorum, reiciendis quas!</p>
				<a href="" class="button">View All of Our education Services</a>
			</div>
		</div>
	</section>
	<section id="support_services">
		<div class="wrapper">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/support_services.jpg" alt=""></figure>
			<div class="text">
				<h2>We Are Here to Help You through the Process</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam perspiciatis natus cumque vero non deserunt, magnam, optio ea dicta nisi dolor magni tenetur voluptatum labore eius dignissimos, qui quisquam nam!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque nesciunt assumenda, illum cumque tenetur saepe asperiores necessitatibus in recusandae porro officia voluptatum vel repellat est rerum deleniti! Nam, eveniet, culpa?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates ab blanditiis, facere assumenda, natus autem necessitatibus similique eveniet error obcaecati consequatur magni reiciendis fuga labore? Sit, aspernatur beatae porro?</p>
				<a href="" class="button">View All of Our Support Services</a>
			</div>
		</div>
	</section>
	<section id="career">
		<div class="wrapper">
			<figure><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/career.jpg" alt=""></figure>
			<div class="text">
				<h2>Need Assistance? We're here to help</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore dicta illo, inventore sit ad, neque harum dolorum sed id est quam consequuntur dolor quae, a maxime aspernatur, quaerat eligendi unde!</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero aspernatur esse dignissimos aut perspiciatis sit labore delectus, explicabo quia. Cupiditate pariatur blanditiis, excepturi vitae, est facilis ratione voluptas praesentium sint.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At laborum quae impedit sit, minus eum accusantium obcaecati in neque illum nesciunt harum nam ratione numquam id quis eligendi ipsam nisi.</p>
				<a href="" class="button">View all of our career services</a>
			</div>
		</div>
	</section>

	<?php ob_start(); ?>
	[aep_parallax id="cta" image="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/students.jpg"]
		<div class="wrapper">
			<h2>Get Started Today!</h2>
			<a href="" class="button">Create Your Profile</a>
		</div>
	[/aep_parallax]
	<?php echo do_shortcode(ob_get_clean()); ?>

	<section id="careers">
		<div class="wrapper">
			<h2>Find The Right Career for You!</h2>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/agriculture.jpg" alt="">
				<figcaption>Agriculture &amp; Natural Resources</figcaption>
			</figure>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/construction.jpg" alt="">
				<figcaption>Building &amp; Construction</figcaption>
			</figure>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ece.jpg" alt="">
				<figcaption>Early Childhood Education</figcaption>
			</figure>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ece.jpg" alt="">
				<figcaption>Hospitality &amp; Culinary Arts</figcaption>
			</figure>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/it.jpg" alt="">
				<figcaption>Information Technology</figcaption>
			</figure>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/agriculture.jpg" alt="">
				<figcaption>Manufacturing &amp; Product Development</figcaption>
			</figure>
		</div>
	</section>

	<?php ob_start(); ?>
	[aep_parallax id="contact" image="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/working-adults.jpg"]
		<div class="wrapper">
			<h2>Got Questions? <small>Contact us Today</small></h2>
			<?php echo do_shortcode('[contact-form-7 id="27" title="Homepage Contact Form"]'); ?>
		</div>
	[/aep_parallax]
	<?php echo do_shortcode(ob_get_clean()); ?>


<?php get_footer(); ?>
