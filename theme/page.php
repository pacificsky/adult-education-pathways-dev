<?php
/**
 * Read up on the WP Template Hierarchy for
 * when this file is used
 *
 */
?>
<?php get_header(); ?>

	<nav class="page--header">
		<div class="wrapper">
			<h1><?php the_title(); ?></h1>
			<?php MOZ_Crumbs::crumbs(); ?>
		</div>
	</nav>

	<?php while(have_posts()): the_post(); ?>
		<main><?php echo wpautop(do_shortcode(get_the_content())); ?></main>
	<?php endwhile; ?>

	<section id="cta">
		<div class="wrapper">
			<h2>Get Started Today!</h2>
			<a href="" class="button">Create Your Profile</a>
		</div>
	</section>

<?php get_footer(); ?>
